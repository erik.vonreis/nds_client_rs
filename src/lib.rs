use thiserror::Error;
use num_complex::Complex32;
use byte_slice_cast::*;
use cxx::UniquePtr;

#[cxx::bridge]
mod ffi {

    enum ChannelType {
        Unknown,
        Online,
        Raw,
        RDS,
        STrend,
        MTrend,
        TestPoint,
        Static,
    }

    enum DataType {
        Unknown,
        Int16,
        Int32,
        Int64,
        Float32,
        Float64,
        Complex32,
        UInt32,
    }

    struct Channel {
        name: String,
        channel_type: ChannelType,
        data_type: DataType,
        sample_rate: f64,
        gain: f32,
        slope: f32,
        offset: f32,
        units: String,
    }

    struct Predicate {
        pattern: String,
        channel_types: Vec<ChannelType>,
        data_types: Vec<DataType>,
        min_sample_rate: f64,
        max_sample_rate: f64,
        gps_start: u64,
        gps_end: u64,
    }

    #[derive(Clone,Copy)]
    struct GPSTime {
        seconds: u64,
        nano: u64,
    }

    unsafe extern "C++" {
        include!("nds2_client_rs/include/nds_client.hh");

        #[namespace = NDS]
        type channel;

        #[namespace = NDS]
        type buffer;

        #[namespace = NDS]
        type data_iterable;


        fn nds_channel_to_rust(ch: &channel) -> Result<Channel>;
        fn nds_buffer_get_channel(buf: &buffer) -> Result<Channel>;
        fn nds_buffer_get_time(buf: &buffer) -> GPSTime;
        fn nds_buffer_get_samples(buf: &buffer) -> usize;
        fn nds_buffer_get_bytes(buf: &buffer) -> &[u8];
        fn nds_iterator_is_done(nds_iter: Pin<&mut data_iterable>) -> bool;
        fn nds_iterator_get_data(nds_iter: Pin<&mut data_iterable>, out: &mut Vec<Buffer>) -> Result<()>;

        fn find_channels(predicate: &Predicate) -> Result<Vec<Channel>>;

        fn fetch(gps_start: u64, gps_end: u64, channels: &Vec<String>, out: &mut Vec<Buffer>) -> Result<()>;

        fn iterate(start: u64, stop: u64, stride: i64, channels: &Vec<String>) -> Result<UniquePtr<data_iterable>>;
    }

    extern "Rust" {
        type Buffer;

        fn nds_buffer_append_to_rust_vec(buf: &buffer, output: &mut Vec<Buffer>) -> Result<()>;
    }
}

fn nds_buffer_append_to_rust_vec(buf: &ffi::buffer, output: &mut Vec<Buffer>) -> Result<(), NDSError> {
    let chan = match ffi::nds_buffer_get_channel(buf) {
        Ok(ch) => ch,
        Err(_) => return Err(NDSError::InvalidInput),
    };
    let time = ffi::nds_buffer_get_time(buf);
    let samples = ffi::nds_buffer_get_samples(buf);
    let data = ffi::nds_buffer_get_bytes(buf);
    let buf = Buffer::new_from_bytes(chan, time, samples, data)?;
    output.push(buf);
    Ok(())
}

impl ffi::Predicate {
    pub fn new() -> Self {
        ffi::Predicate{
            pattern: String::from("*"),
            channel_types: Vec::new(),
            data_types: Vec::new(),
            min_sample_rate: 0.0,
            max_sample_rate: f64::MAX,
            gps_start: 0,
            gps_end: 1999999999,
        }
    }

    pub fn channel_type(&mut self, type_val: ChannelType) -> &mut Self {
        self.channel_types.push(type_val);
        self
    }

    pub fn data_type(&mut self, type_val: DataType) -> &mut Self {
        self.data_types.push(type_val);
        self
    }
}

impl Display for ffi::ChannelType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{0}", match *self {
            ChannelType::Online => "online",
            ChannelType::Raw => "raw",
            ChannelType::RDS => "rds",
            ChannelType::STrend => "s-trend",
            ChannelType::MTrend => "m-trend",
            ChannelType::TestPoint => "test-point",
            ChannelType::Static => "static",
            _ => "unknown",
        })
    }
}

impl Display for ffi::DataType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{0}", match *self {
            DataType::Int16 => "int16",
            DataType::Int32 => "int32",
            DataType::Int64 => "int64",
            DataType::Float32 => "float32",
            DataType::Float64 => "float64",
            DataType::Complex32 => "complex",
            DataType::UInt32 => "uint32",
            _ => "unknown",
        })
    }
}

impl Display for ffi::Channel {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<{0} {1} {2} {3}>", self.name, self.sample_rate, self.data_type, self.channel_type)
    }
}

use std::fmt::{Display, Formatter};
pub use ffi::{
    Channel,
    ChannelType,
    DataType,
    GPSTime,
    Predicate,
    find_channels,
};

pub enum Data {
    Int16(Vec<i16>),
    Int32(Vec<i32>),
    Int64(Vec<i64>),
    Float32(Vec<f32>),
    Float64(Vec<f64>),
    Complex32(Vec<Complex32>),
    UInt32(Vec<u32>),
    Unknown((Vec<u8>, usize)),
}

fn data_element_size(dt: DataType) -> usize {
    match dt {
        DataType::Int16 => 2,
        DataType::Int32 => 4,
        DataType::Int64 => 8,
        DataType::Float32 => 4,
        DataType::Float64 => 8,
        DataType::Complex32 => 4 + 4,
        DataType::UInt32 => 4,
        DataType::Unknown => 0,
        _ => 0,
    }
}

pub struct Buffer {
    pub channel: Channel,
    pub time: GPSTime,
    pub data: Data,
}

#[derive(Error, Debug)]
pub enum NDSError {
    #[error("function passed invalid input")]
    InvalidInput,
    #[error("Unknown error while doing a fetch")]
    FetchError,
    #[error("Unknown error while doing an interate")]
    IterateError,
}

fn bytes_to_data(data_type: DataType, samples: usize, bytes: &[u8]) -> Result<Data, Box<dyn std::error::Error>> {
    let size = data_element_size(data_type);
    let d =  if data_type != DataType::Unknown && size != 0 {
        match data_type {
            DataType::Int16 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<i16>()?;
                v.resize(samples, 0);
                v.copy_from_slice(b);
                Data::Int16(v)
            },
            DataType::Int32 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<i32>()?;
                v.resize(samples, 0);
                v.copy_from_slice(b);
                Data::Int32(v)
            },
            DataType::Int64 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<i64>()?;
                v.resize(samples, 0);
                v.copy_from_slice(b);
                Data::Int64(v)
            },
            DataType::Float32 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<f32>()?;
                v.resize(samples, 0.0);
                v.copy_from_slice(b);
                Data::Float32(v)
            },
            DataType::Float64 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<f64>()?;
                v.resize(samples, 0.0);
                v.copy_from_slice(b);
                Data::Float64(v)
            },
            DataType::Complex32 => {
                let b = bytes.as_slice_of::<f32>()?;
                let mut i1 = b.iter().step_by(2);
                let mut i2 = b.iter().skip(1).step_by(2);
                let v = i1.zip(i2).map(|val| -> Complex32 {
                    Complex32::new(*val.0, *val.1)
                }).collect::<Vec<Complex32>>();
                Data::Complex32(v)
            }
            DataType::UInt32 => {
                let mut v = Vec::new();
                let b = bytes.as_slice_of::<u32>()?;
                v.resize(samples, 0);
                v.copy_from_slice(b);
                Data::UInt32(v)
            },
            _ => {
                return Err(Box::new(NDSError::InvalidInput));
            }
        }
    } else {
        let mut v = Vec::new();
        v.resize(bytes.len(), 0);
        v.copy_from_slice(bytes);
        Data::Unknown((v, samples))
    };
    Ok(d)
}

impl Buffer {
    fn new_from_bytes(channel: Channel, time: GPSTime, samples: usize, bytes: &[u8]) -> Result<Buffer, NDSError> {
        let d = match bytes_to_data(channel.data_type, samples, bytes) {
            Ok(data) => data,
            Err(_) => return Err(NDSError::InvalidInput),
        };
        Ok(Buffer{
            channel,
            time,
            data: d,
        })
    }

    pub fn samples(&self) -> usize {
        match &self.data {
            Data::Int16(v) => v.len(),
            Data::Int32(v) => v.len(),
            Data::Int64(v) => v.len(),
            Data::Float32(v) => v.len(),
            Data::Float64(v) => v.len(),
            Data::Complex32(v) => v.len(),
            Data::UInt32(v) => v.len(),
            Data::Unknown((_, size)) => *size,
        }
    }

    pub fn start(&self) -> GPSTime {
        self.time.clone()
    }

    pub fn stop(&self) -> GPSTime {
        let samples = self.samples();
        let total_ns = self.time.nano + self.samples_to_trailing_nanoseconds( samples );
        let additional_from_nano = if total_ns > 1000000000 {
            1
        } else {
            0
        };
        GPSTime{
            seconds: self.time.seconds + self.samples_to_seconds(samples) + additional_from_nano,
            nano: self.time.nano,
        }
    }

    fn samples_to_seconds(&self, count: usize) -> u64 {
        if let ChannelType::MTrend = self.channel.channel_type {
            return count as u64 * 60;
        }
        return (count as u64) / (self.channel.sample_rate as u64);
    }

    fn samples_to_trailing_nanoseconds(&self, count: usize) -> u64 {
        if let ChannelType::MTrend = self.channel.channel_type {
            return 0;
        }
        let samples = count % self.channel.sample_rate as usize;
        (1e+9 * samples as f64 / self.channel.sample_rate) as u64
    }
}

pub fn fetch(gps_start: u64, gps_end: u64, channels: &Vec<String>) -> Result<Vec<Buffer>, NDSError> {
    let mut bufs = Vec::new();
    if let Err(_) = ffi::fetch(gps_start, gps_end, channels, &mut bufs) {
        return Err(NDSError::FetchError);
    }
    Ok(bufs)
}

pub enum Stride {
    Seconds(u32),
    Fast,
}

impl Stride {
    fn to_nds_stride(&self) -> i64 {
        match &self {
            Stride::Seconds(s) => *s as i64,
            Stride::Fast => -1,
        }
    }
}

pub struct NDSIterator {
    handle: UniquePtr<ffi::data_iterable>,
}

impl Iterator for NDSIterator {
    type Item = Vec<Buffer>;

    fn next(&mut self) -> Option<Self::Item> {
        if ffi::nds_iterator_is_done(self.handle.pin_mut()) {
            return None;
        }
        let mut out = Vec::new();
        match ffi::nds_iterator_get_data(self.handle.pin_mut(), &mut out) {
            Ok(_) => Some(out),
            Err(_) => None,
        }
    }
}

pub fn iterate(gps_start: u64, gps_end: u64, stride: Stride, channels: &Vec<String>) -> Result<NDSIterator, NDSError> {
    match ffi::iterate(gps_start, gps_end, stride.to_nds_stride(), channels) {
        Ok(handle) => Ok(NDSIterator{
            handle
        }),
        Err(_) => Err(NDSError::IterateError),
    }
}