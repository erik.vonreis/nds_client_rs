#include "nds2_client_rs/include/nds_client.hh"
#include "nds2_client_rs/src/lib.rs.h"

#include <algorithm>
#include <iterator>
#include <string>

namespace
{
    std::vector<std::string>
    to_nds_channel_name_list(const rust::Vec<rust::String>& channels)
    {
        std::vector<std::string> chans{};
        chans.reserve(channels.size());
        std::transform(channels.begin(), channels.end(), std::back_inserter(chans), [](const rust::String& str) -> std::string {
            return std::string(str.begin(), str.end());
        });
        return chans;
    }

    NDS::channel::channel_type
    rust_to_nds(ChannelType val)
    {
        switch (val)
        {
            case ChannelType::Online: return NDS::channel::channel_type::CHANNEL_TYPE_ONLINE;
            case ChannelType::Raw: return NDS::channel::CHANNEL_TYPE_RAW;
            case ChannelType::RDS: return NDS::channel::CHANNEL_TYPE_RDS;
            case ChannelType::STrend: return NDS::channel::CHANNEL_TYPE_STREND;
            case ChannelType::MTrend: return NDS::channel::CHANNEL_TYPE_MTREND;
            case ChannelType::TestPoint: return NDS::channel::CHANNEL_TYPE_TEST_POINT;
            case ChannelType::Static: return NDS::channel::CHANNEL_TYPE_STATIC;
            default: return NDS::channel::CHANNEL_TYPE_UNKNOWN;
        }
    }

    NDS::channel::data_type
    rust_to_nds(DataType val)
    {
        switch (val)
        {
            case DataType::Int16: return NDS::channel::DATA_TYPE_INT16;
            case DataType::Int32: return NDS::channel::DATA_TYPE_INT32;
            case DataType::Int64: return NDS::channel::DATA_TYPE_INT64;
            case DataType::Float32: return NDS::channel::DATA_TYPE_FLOAT32;
            case DataType::Float64: return NDS::channel::DATA_TYPE_FLOAT64;
            case DataType::Complex32: return NDS::channel::DATA_TYPE_COMPLEX32;
            case DataType::UInt32: return NDS::channel::DATA_TYPE_UINT32;
            default: return NDS::channel::DATA_TYPE_UNKNOWN;
        }
    }
}

Channel nds_channel_to_rust(const NDS::channel& ch)
{
    Channel chan{};
    chan.name = ch.Name();
    switch (ch.Type()) {
    case NDS::channel::CHANNEL_TYPE_ONLINE:
        chan.channel_type = ChannelType::Online;
        break;
    case NDS::channel::CHANNEL_TYPE_RAW:
        chan.channel_type = ChannelType::Raw;
        break;
    case NDS::channel::CHANNEL_TYPE_RDS:
        chan.channel_type = ChannelType::RDS;
        break;
    case NDS::channel::CHANNEL_TYPE_STREND:
        chan.channel_type = ChannelType::STrend;
        break;
    case NDS::channel::CHANNEL_TYPE_MTREND:
        chan.channel_type = ChannelType::MTrend;
        break;
    case NDS::channel::CHANNEL_TYPE_TEST_POINT:
        chan.channel_type = ChannelType::TestPoint;
        break;
    case NDS::channel::CHANNEL_TYPE_STATIC:
        chan.channel_type = ChannelType::Static;
        break;
    default:
        chan.channel_type = ChannelType::Unknown;
        break;
    }
    switch (ch.DataType()) {
    case NDS::channel::data_type::DATA_TYPE_INT16:
        chan.data_type = DataType::Int16;
        break;
    case NDS::channel::data_type::DATA_TYPE_INT32:
        chan.data_type = DataType::Int32;
        break;
    case NDS::channel::data_type::DATA_TYPE_INT64:
        chan.data_type = DataType::Int64;
        break;
    case NDS::channel::data_type::DATA_TYPE_FLOAT32:
        chan.data_type = DataType::Float32;
        break;
    case NDS::channel::data_type::DATA_TYPE_FLOAT64:
        chan.data_type = DataType::Float64;
        break;
    case NDS::channel::data_type::DATA_TYPE_COMPLEX32:
        chan.data_type = DataType::Complex32;
        break;
    case NDS::channel::data_type::DATA_TYPE_UINT32:
        chan.data_type = DataType::UInt32;
        break;
    default:
        chan.data_type = DataType::Unknown;
        break;
    }

    chan.sample_rate = ch.SampleRate();
    chan.gain = ch.Gain();
    chan.slope = ch.Slope();
    chan.offset = ch.Offset();
    chan.units = ch.Units();

    return chan;
}

Channel
nds_buffer_get_channel(const NDS::buffer& buf)
{
    return nds_channel_to_rust(buf);
}

GPSTime
nds_buffer_get_time(const NDS::buffer& buf)
{
    GPSTime t{};
    t.seconds = buf.Start();
    t.nano = buf.StartNano();
    return t;
}

void
nds_iterator_get_data(NDS::data_iterable& nds_iter, rust::Vec<Buffer>& out)
{
    if (nds_iterator_is_done(nds_iter))
    {
        throw std::runtime_error("iterator complete");
    }
    auto cur = nds_iter.begin();
    auto shared_bufs = *cur;
    if (!shared_bufs)
    {
        throw std::runtime_error("invalid buffers");
    }
    out.clear();
    out.reserve(shared_bufs->size());
    for (const auto& buf:*shared_bufs)
    {
        nds_buffer_append_to_rust_vec(buf, out);
    }
    ++cur;
}

rust::Vec<::Channel> find_channels(const Predicate& predicate) {
    std::string c_pattern(predicate.pattern.begin(), predicate.pattern.end());
    auto pred = NDS::channel_predicate(
        c_pattern,
        NDS::frequency_range(predicate.min_sample_rate, predicate.max_sample_rate),
        NDS::epoch(predicate.gps_start, predicate.gps_end)
    );
    for (const auto& entry: predicate.channel_types)
    {
        pred.set(rust_to_nds(entry));
    }
    for (const auto& entry: predicate.data_types)
    {
        pred.set(rust_to_nds(entry));
    }
    auto c_vec = NDS::find_channels(pred);
    rust::Vec<::Channel> out{};

    std::for_each(c_vec.begin(), c_vec.end(), [&out](const NDS::channel &ch) {
        out.emplace_back(nds_channel_to_rust(ch));
    });
    return out;
};

void
fetch(std::uint64_t gps_start, std::uint64_t gps_end, const rust::Vec<rust::String>& channels, rust::Vec<Buffer>& out)
{
    std::vector<std::string> chans = to_nds_channel_name_list(channels);
    auto buffers = NDS::fetch(gps_start, gps_end, chans);
    out.clear();
    out.reserve(buffers.size());
    for (const auto& buf:buffers)
    {
        nds_buffer_append_to_rust_vec(buf, out);
    }
}

std::unique_ptr<NDS::data_iterable>
iterate(std::uint64_t start, std::uint64_t stop, std::int64_t stride, const rust::Vec<rust::String>& channels)
{
    std::vector<std::string> chans = to_nds_channel_name_list(channels);
    NDS::request_period period(start, stop, stride);
    return std::make_unique<NDS::data_iterable>(NDS::iterate(period, chans));
}