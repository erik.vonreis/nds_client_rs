use nds2_client_rs::{ChannelType, DataType, Predicate, fetch, find_channels, Data};

fn main() {
    let mut pred = Predicate::new();
    pred.pattern = String::from("H1:PEM*");
    pred.channel_type(ChannelType::Raw).data_type( DataType::Float32 );
    pred.max_sample_rate = 2048.0;
    let chans = find_channels(&pred).unwrap();
    chans.iter().for_each(|ch| {
        println!("{0}", ch)
    });
    let mut channels = Vec::new();
    channels.push(String::from("H1:FEC-28_CPU_METER"));
    let bufs = fetch(1391790650, 1391790656, &channels).unwrap();
    println!("Got {0} channels back", bufs.len());
    bufs.iter().for_each(|b| {
        println!("{0} ({1} - {2}) ", b.channel.name, b.time.seconds, b.stop().seconds);
        match &b.data {
            Data::Float32(v) => {
                v.iter().take(10).for_each(|e| {
                    print!(" {0}", e);
                });
                println!();
            }
            Data::Int32(v) => {
                v.iter().take(10).for_each(|e| {
                    print!(" {0}", e);
                });
                println!();
            }
            _ => println!("Skipping data")
        }
    });
}