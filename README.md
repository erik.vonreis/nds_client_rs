nds2_client_rs

A simplified test rust binding on the standalone NDS client functions.

This supports find_channels and fetch currently.

Major issues:

 * Not sure if the output structures are right
 * Not sure if the crate name is right
 * Errors being returned are very bad