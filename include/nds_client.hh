#ifndef NDS2_CLIENT_RUST_WRAPPER_HH
#define NDS2_CLIENT_RUST_WRAPPER_HH

#include <nds.hh>
#include "rust/cxx.h"

class Buffer;
class Channel;
class Predicate;
class GPSTime;

Channel nds_channel_to_rust(const NDS::channel& chan);

Channel nds_buffer_get_channel(const NDS::buffer& buf);

GPSTime nds_buffer_get_time(const NDS::buffer& buf);

inline std::size_t
nds_buffer_get_samples(const NDS::buffer& buf)
{
    return buf.Samples();
}

inline rust::Slice<const unsigned char>
nds_buffer_get_bytes(const NDS::buffer& buf)
{
    const unsigned char* start = reinterpret_cast<const unsigned char*>(buf.cbegin<void>());
    const unsigned char* end = reinterpret_cast<const unsigned char*>(buf.cend<void>());
    return rust::Slice<const unsigned char>(start, end-start);
}

inline bool
nds_iterator_is_done(NDS::data_iterable& nds_iter)
{
    return nds_iter.begin() == nds_iter.end();
}


void
nds_iterator_get_data(NDS::data_iterable& nds_iter, rust::Vec<Buffer>& out);

rust::Vec<Channel> find_channels(const Predicate& predicate);

void
fetch(std::uint64_t gps_start, std::uint64_t gps_end, const rust::Vec<rust::String>& channels, rust::Vec<Buffer>& out);

std::unique_ptr<NDS::data_iterable>
iterate(std::uint64_t start, std::uint64_t stop, std::int64_t stride, const rust::Vec<rust::String>& channels);

#endif // NDS2_CLIENT_RUST_WRAPPER_HH